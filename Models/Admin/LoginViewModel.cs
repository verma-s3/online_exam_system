﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_Exam_System.Models.Admin
{
    public class LoginViewModel
    {
        [EmailAddress]
        [Required(ErrorMessage = "Please Enter the Email Address"), MaxLength(100)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please Enter the Password"), MinLength(8), MaxLength(15)]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }

    }
}
