﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_Exam_System.Models.Admin
{
    public class Javascript
    {
        [Key]
        public int Id { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the Question"), MaxLength(100)]
        [Display(Name = "Question")]
        public string Question { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the Correct Answer"), MaxLength(100)]
        [Display(Name = "Correct Answer")]
        public string CorrectAnswer { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the Answer 1"), MaxLength(100)]
        [Display(Name = "Answer 1")]
        public string Ans1 { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the Answer 2"), MaxLength(100)]
        [Display(Name = "Answer 2")]
        public string Ans2 { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the Answer 3"), MaxLength(100)]
        [Display(Name = "Answer 3")]
        public string Ans3 { get; set; }
    }
}
