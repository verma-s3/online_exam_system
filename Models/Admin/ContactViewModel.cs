﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Online_Exam_System.Models.Admin
{
    public class ContactViewModel
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the First Name"), MaxLength(100)]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the Last Name"), MaxLength(100)]
        public string LastName { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Please Enter the Email Address")]
        public string Email { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(500)]
        public string Message { get; set; }
    }
}
