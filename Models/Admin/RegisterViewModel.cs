﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_Exam_System.Models.Admin
{
    public class RegisterViewModel
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please Enter the User Name"), MaxLength(100)]
        public string UserName { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Please Enter the Email Address"), MaxLength(100)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please Enter the Password"),MinLength(8), MaxLength(15)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="Password and Confirm Password do not match. Please enter again!!")]
        [Required(ErrorMessage = "Please Enter the Confirm Password"), MinLength(8), MaxLength(15)]
        public string ConfirmPassword { get; set; }
    }
}
