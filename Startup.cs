﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Data;
using Online_Exam_System.Models;
using Microsoft.AspNetCore.Identity;

namespace Online_Exam_System
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddDbContext<dbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("dbContext")));
            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<dbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Admin",
                    pattern: "{controller=Admin}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Html",
                    pattern: "{controller=Html}/{action=Admin/Html/Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Css",
                    pattern: "{controller=Css}/{action=Admin/Css/Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Javascript",
                    pattern: "{controller=Javascript}/{action=Admin/Javascript/Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Php",
                    pattern: "{controller=Php}/{action=Admin/Php/Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "HtmlExam",
                    pattern: "{controller=Html}/{action=HtmlExamimation}/{id?}");
                /*endpoints.MapControllerRoute("Admin", "Admin/{controller}/{action}",new { 
                    controller="Html", action="index"
                });*/
            });
        }
    }
}
