﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Online_Exam_System.Models.Admin.Html;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Data;

namespace Online_Exam_System.Controllers
{
    public class HtmlExamController : Controller
    {
        public readonly dbContext _context;

        public HtmlExamController(dbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> HtmlExamimation()
        {
            var results = await _context.Html.ToListAsync();
            return View("~/Views/HtmlExam/HtmlExamination.cshtml",results);
        }
    }
}
