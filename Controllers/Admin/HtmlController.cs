﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Data;
using Online_Exam_System.Models.Admin.Html;

namespace Online_Exam_System.Controllers.Admin
{
    public class HtmlController : Controller
    {
        private readonly dbContext _context;

        public HtmlController(dbContext context)
        {
            _context = context;
        }

        // GET: Admin/Html
        public async Task<IActionResult> Index()
        {
            var data = await _context.Html.ToListAsync();
            return View("~/Views/Admin/Html/Index.cshtml",data);
            /*return View(await _context.Html.ToListAsync());*/
        }

       
        public async Task<IActionResult> HtmlExamimation()
        {
            var data = await _context.Html.ToListAsync();

            //create a random renerate values with random object

            return View("~/Views/HtmlExam/HtmlExamimation.cshtml",data);
        }

        public IActionResult HtmlResult()
        {
            var data = _context.Html.ToList();
            return View("~/Views/Html/HtmlResult.cshtml", data);
        }

        // GET: Admin/Html/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var html = await _context.Html
                .FirstOrDefaultAsync(m => m.Id == id);
            if (html == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Html/Details.cshtml",html);
        }

        // GET: Admin/Html/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/Html/Create.cshtml");
        }

        // POST: Admin/Html/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Html html)
        {
            if (ModelState.IsValid)
            {
                _context.Add(html);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Html/Create.cshtml",html);
        }

        // GET: Admin/Html/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var html = await _context.Html.FindAsync(id);
            if (html == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/Html/Edit.cshtml",html);
        }

        // POST: Admin/Html/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Html html)
        {
            if (id != html.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(html);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HtmlExists(html.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Html/Edit.cshtml",html);
        }

        // GET: Admin/Html/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var html = await _context.Html
                .FirstOrDefaultAsync(m => m.Id == id);
            if (html == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Html/Delete.cshtml",html);
        }

        // POST: Admin/Html/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var html = await _context.Html.FindAsync(id);
            _context.Html.Remove(html);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HtmlExists(int id)
        {
            return _context.Html.Any(e => e.Id == id);
        }
    }
}
