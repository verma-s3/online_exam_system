﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Data;
using Online_Exam_System.Models.Admin;

namespace Online_Exam_System.Controllers.Admin
{
    public class PhpController : Controller
    {
        private readonly dbContext _context;

        public PhpController(dbContext context)
        {
            _context = context;
        }

        // GET: Php
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/Php/Index.cshtml",await _context.Php.ToListAsync());
        }

        public async Task<IActionResult> PhpExamination()
        {
            var data = await _context.Php.ToListAsync();

            //create a random renerate values with random object

            return View("~/Views/PhpExam/PhpExamination.cshtml", data);
        }

        public IActionResult PhpResult()
        {
            var data = _context.Php.ToList();
            return View("~/Views/Php/PhpResult.cshtml", data);
        }

        // GET: Php/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var php = await _context.Php
                .FirstOrDefaultAsync(m => m.Id == id);
            if (php == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Php/Details.cshtml",php);
        }

        // GET: Php/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/Php/Create.cshtml");
        }

        // POST: Php/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Php php)
        {
            if (ModelState.IsValid)
            {
                _context.Add(php);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Php/Create.cshtml",php);
        }

        // GET: Php/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var php = await _context.Php.FindAsync(id);
            if (php == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/Php/Edit.cshtml",php);
        }

        // POST: Php/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Php php)
        {
            if (id != php.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(php);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PhpExists(php.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Php/Edit.cshtml",php);
        }

        // GET: Php/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var php = await _context.Php
                .FirstOrDefaultAsync(m => m.Id == id);
            if (php == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Php/Delete.cshtml",php);
        }

        // POST: Php/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var php = await _context.Php.FindAsync(id);
            _context.Php.Remove(php);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PhpExists(int id)
        {
            return _context.Php.Any(e => e.Id == id);
        }
    }
}
