﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Data;
using Online_Exam_System.Models.Admin;

namespace Online_Exam_System.Controllers.Admin
{
    public class CssController : Controller
    {
        private readonly dbContext _context;

        public CssController(dbContext context)
        {
            _context = context;
        }

        // GET: Css
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/Css/Index.cshtml", await _context.Css.ToListAsync());
        }

        public async Task<IActionResult> CssExamination()
        {
            var data = await _context.Css.ToListAsync();

            //create a random renerate values with random object

            return View("~/Views/CssExam/CssExamination.cshtml", data);
        }

        public IActionResult CssResult()
        {
            var data = _context.Css.ToList();
            return View("~/Views/Css/CssResult.cshtml", data);
        }

        // GET: Css/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var css = await _context.Css
                .FirstOrDefaultAsync(m => m.Id == id);
            if (css == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Css/Details.cshtml",css);
        }

        // GET: Css/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/Css/Create.cshtml");
        }

        // POST: Css/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Css css)
        {
            if (ModelState.IsValid)
            {
                _context.Add(css);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Css/Create.cshtml",css);
        }

        // GET: Css/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var css = await _context.Css.FindAsync(id);
            if (css == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/Css/Edit.cshtml",css);
        }

        // POST: Css/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Css css)
        {
            if (id != css.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(css);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CssExists(css.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Css/Edit.cshtml",css);
        }

        // GET: Css/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var css = await _context.Css
                .FirstOrDefaultAsync(m => m.Id == id);
            if (css == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Css/Delete.cshtml",css);
        }

        // POST: Css/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var css = await _context.Css.FindAsync(id);
            _context.Css.Remove(css);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CssExists(int id)
        {
            return _context.Css.Any(e => e.Id == id);
        }
    }
}
