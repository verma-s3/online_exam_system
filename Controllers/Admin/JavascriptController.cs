﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Data;
using Online_Exam_System.Models.Admin;

namespace Online_Exam_System.Controllers.Admin
{
    public class JavascriptController : Controller
    {
        private readonly dbContext _context;

        public JavascriptController(dbContext context)
        {
            _context = context;
        }

        // GET: Admin/Javascript
        public async Task<IActionResult> Index()
        {
            return View("~/Views/Admin/Javascript/Index.cshtml", await _context.Javascript.ToListAsync());
        }

        public async Task<IActionResult> JavascriptExamination()
        {
            var data = await _context.Javascript.ToListAsync();

            //create a random renerate values with random object

            return View("~/Views/JavascriptExam/JavascriptExamination.cshtml", data);
        }

        public IActionResult JavascriptResult()
        {
            var data = _context.Javascript.ToList();
            return View("~/Views/Javascript/JavascriptResult.cshtml", data);
        }

        // GET: Admin/Javascript/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var javascript = await _context.Javascript
                .FirstOrDefaultAsync(m => m.Id == id);
            if (javascript == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Javascript/Details.cshtml", javascript);
        }

        // GET: Admin/Javascript/Create
        public IActionResult Create()
        {
            return View("~/Views/Admin/Javascript/Create.cshtml");
        }

        // POST: Admin/Javascript/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Javascript javascript)
        {
            if (ModelState.IsValid)
            {
                _context.Add(javascript);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Javascript/Create.cshtml",javascript);
        }

        // GET: Admin/Javascript/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var javascript = await _context.Javascript.FindAsync(id);
            if (javascript == null)
            {
                return NotFound();
            }
            return View("~/Views/Admin/Javascript/Edit.cshtml",javascript);
        }

        // POST: Admin/Javascript/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Question,CorrectAnswer,Ans1,Ans2,Ans3")] Javascript javascript)
        {
            if (id != javascript.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(javascript);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JavascriptExists(javascript.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Admin/Javascript/Edit.cshtml", javascript);
        }

        // GET: Admin/Javascript/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var javascript = await _context.Javascript
                .FirstOrDefaultAsync(m => m.Id == id);
            if (javascript == null)
            {
                return NotFound();
            }

            return View("~/Views/Admin/Javascript/Delete.cshtml", javascript);
        }

        // POST: Admin/Javascript/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var javascript = await _context.Javascript.FindAsync(id);
            _context.Javascript.Remove(javascript);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JavascriptExists(int id)
        {
            return _context.Javascript.Any(e => e.Id == id);
        }
    }
}
