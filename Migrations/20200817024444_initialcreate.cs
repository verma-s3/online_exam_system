﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Online_Exam_System.Migrations
{
    public partial class initialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Css",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Question = table.Column<string>(maxLength: 100, nullable: false),
                    CorrectAnswer = table.Column<string>(maxLength: 100, nullable: false),
                    Ans1 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans2 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans3 = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Css", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Html",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Question = table.Column<string>(maxLength: 100, nullable: false),
                    CorrectAnswer = table.Column<string>(maxLength: 100, nullable: false),
                    Ans1 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans2 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans3 = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Html", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Javascript",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Question = table.Column<string>(maxLength: 100, nullable: false),
                    CorrectAnswer = table.Column<string>(maxLength: 100, nullable: false),
                    Ans1 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans2 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans3 = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Javascript", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Php",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Question = table.Column<string>(maxLength: 100, nullable: false),
                    CorrectAnswer = table.Column<string>(maxLength: 100, nullable: false),
                    Ans1 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans2 = table.Column<string>(maxLength: 100, nullable: false),
                    Ans3 = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Php", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Css");

            migrationBuilder.DropTable(
                name: "Html");

            migrationBuilder.DropTable(
                name: "Javascript");

            migrationBuilder.DropTable(
                name: "Php");
        }
    }
}
