﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Online_Exam_System.Models.Admin.Html;
using Online_Exam_System.Models.Admin;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Online_Exam_System.Models;

namespace Online_Exam_System.Data
{
    public class dbContext : IdentityDbContext
    {
        /*private DbContextOptions<HtmlExam> options;*/

        public dbContext (DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        /*public dbContext(DbContextOptions<HtmlExam> options)
        {
            this.options = options;
        }*/

        public DbSet<Online_Exam_System.Models.Admin.Html.Html> Html { get; set; }

        public DbSet<Online_Exam_System.Models.Admin.Css> Css { get; set; }

        public DbSet<Online_Exam_System.Models.Admin.Javascript> Javascript { get; set; }

        public DbSet<Online_Exam_System.Models.Admin.Php> Php { get; set; }
    }
}
